# Adquirindo os dados

Recolheremos os dados do arquivo new_thyroid.csv utilizando a biblioteca pandas. 
Importados os dados, exibiremos suas respectivas dimensoes.

<img width="250px" src="https://media.tenor.com/PT2cX4YdvWYAAAAd/research-lucky-star.gif"/>


```python
import pandas as pd

data = pd.read_csv('new_thyroid.csv', header=None)
print(f'shape: {data.shape}')
```

    shape: (215, 6)


## Separando os rotulos

Separaremos os atributos dos rotulos dos dados inicialmente coletada. Os rotulos sao a sexta coluna e os atributos as demais colunas.
Separados os rotulos exibiremos suas respectivas dimensoes.

<img width='400px' src='https://media.tenor.com/ZtuVwa_2f1oAAAAC/kobayashi-san-chi-no-maid-dragon-anime.gif' />



```python
y = data[5]
X = data.iloc[:,0:5]

print(f'Rotulos shape: {y.shape}')
print(f'Atributos shape: {X.shape}')
```

    Rotulos shape: (215,)
    Atributos shape: (215, 5)



```python
print(f'Rotulos:\n{y[:3]}')
```

    Rotulos:
    0    1
    1    1
    2    1
    Name: 5, dtype: int64


# Visualizacao dos dados

Separados os atributos dos rotulos, realizaremos uma visualizacao dos dados por meio de graficos de pares.

<img width='250px' src='https://media.tenor.com/st6FlnvKL9MAAAAM/inugami-korone-processing.gif'/>


```python
import warnings

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

%matplotlib inline

warnings.filterwarnings("ignore")

df = pd.DataFrame(X)
df["Classe"] = np.choose(y, ["","normal","hyper","hipo"])

sns.pairplot(df, hue="Classe", kind="scatter", markers=["o", "s", "D"])

plt.show()
```


![png](README_files/README_6_0.png)


# Conjunto de treinamento e de teste

Depois de separados os rotulos e os atributos separaremos nosso conjunto de dados entre dados de treinamento e teste em .20 para teste .80 para treinamento usando a funcao train_test_split.

<img width='450px' src='https://media.tenor.com/iy0HRiY_6sgAAAAd/gura-gawr-gura.gif'/>


```python
from sklearn.model_selection import train_test_split

np.random.seed(0)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.20)

print(f'X_train: {X_train.shape}')
print(f'X_test: {X_test.shape}')
print(f'y_train: {y_train.shape}')
print(f'y_test: {y_test.shape}')
```

    X_train: (172, 5)
    X_test: (43, 5)
    y_train: (172,)
    y_test: (43,)


# Classificador KNN

Utilizaremos o classificador KNN (k-Nearest Neighbors) para a previsao dos novos dados.

<img width='450px' src='https://i.pinimg.com/originals/87/1e/ae/871eae5549abba60f48bffaf762aec6e.gif'/>

## KNN: n_neighbors=1

Primeiramente utilizaremos o parametro *n_neighbors=1*. Esse parametro diz respeito a quantidade de vizinhos mais proximos do ponto a ser previsto.


```python
from sklearn.neighbors import KNeighborsClassifier

knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)
```

### Avaliacao do Modelo: KNN(n_neighbors=1)
Agora avaliaremos a acuracia do modelo por meio do conjunto de testes, para o modelo com hiperparametro *k=1*

<img width='500px' src='./README_files/8no1.gif'/>


```python
y_pred = knn.predict(X_test)

print(f'Pontuação no conjunto de teste: {knn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste: 0.98


## KNN: k=5
Agora testaremos mudar o hiperparemetro k para 5 e realizaremos sua respectiva avaliacao.

<img width='450px' src='./README_files/luffygear5.gif'/>


```python
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)

print(f'Pontuação no conjunto de teste (k=5): {knn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste (k=5): 0.98


## KNN: k=3
Finalmente realizaremos o ultimo teste para k=3 e o avaliaremos.

<img width='450px' src='https://i.pinimg.com/originals/db/19/fb/db19fb55decc074440b5afd115016b41.gif'/>


```python
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)

print(f'Pontuação no conjunto de teste (k=3): {knn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste (k=3): 1.00


## Melhor avaliacao

Dos tres testes anteriores, o valor do parametro n_neighbors com melhor resultado para o conjunto de teste foi igual a 3, com 100% de precisao.


# Classificador Radius KNN

Agora treinaremos o Classificador Radius KNN e realizaremos a previsao das classes das amostras de teste para tres valores diferentes para o raio:
- radius = 100
- radius = 40
- radius = 20

<img width='400px' src='https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/32918795-ca63-439a-abff-66e9c2b4fa72/ddmsmog-2a898608-31a6-4fae-a148-a2aa2ac59b66.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzMyOTE4Nzk1LWNhNjMtNDM5YS1hYmZmLTY2ZTljMmI0ZmE3MlwvZGRtc21vZy0yYTg5ODYwOC0zMWE2LTRmYWUtYTE0OC1hMmFhMmFjNTliNjYuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.mGnIES7GLxqjuEfSCvcsmIhY0pQGdgXFvsL3Km3EcDo'/>


```python
from sklearn.neighbors import RadiusNeighborsClassifier

rnn = RadiusNeighborsClassifier(radius=100)
rnn.fit(X_train, y_train)

y_pred = rnn.predict(X_test)

print(f'Pontuação no conjunto de teste (r=100): {rnn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste (r=100): 0.65



```python
rnn = RadiusNeighborsClassifier(radius=40)
rnn.fit(X_train, y_train)

y_pred = rnn.predict(X_test)

print(f'Pontuação no conjunto de teste (r=40): {rnn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste (r=40): 0.70



```python
rnn = RadiusNeighborsClassifier(radius=20)
rnn.fit(X_train, y_train)

y_pred = rnn.predict(X_test)

print(f'Pontuação no conjunto de teste (r=20): {rnn.score(X_test, y_test):.2f}')
```

    Pontuação no conjunto de teste (r=20): 0.86


## Melhor Avaliacao

Dos tres testes realizados anteriormente o parametro radius com melhor acuracia para o conjunto de testes tem valor 20, com 86% de precisao.

# Obrigado 

<img width='450px' src='https://gifs.eco.br/wp-content/uploads/2022/02/animados-gifs-do-chopper-de-one-piece-8.gif'>
